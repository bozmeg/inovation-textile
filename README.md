## SAXSHIRT PROJECT

### intro

|Field|responsibility|
|---------------------------------------------|----|
|[Choice of sensors](https://gitlab.com/bozmeg/inovation-textile/wikis/Choice-of-sensors)                                                                       |Both|
|[Choice of microcontroller](https://gitlab.com/bozmeg/inovation-textile/wikis/Choice-of-microcontroller)                                                       |Ohad|
|[Wiring](https://gitlab.com/bozmeg/inovation-textile/wikis/Wiring)                                                                                             |Both|
|[Connecting mechanism](https://gitlab.com/bozmeg/inovation-textile/wikis/Connecting-mechanism)                                                                 |Iman|
|[Powering](https://gitlab.com/bozmeg/inovation-textile/wikis/Powering)                                                                                         |Both|
|[Communication between components and external base](https://gitlab.com/bozmeg/inovation-textile/wikis/Communication-between-components-and-external-base)     |Ohad|
|[App for monitoring](https://gitlab.com/bozmeg/inovation-textile/wikis/App-for-monitoring)                                                                     |Ohad|
|[Textile](https://gitlab.com/bozmeg/inovation-textile/wikis/Textile)                                                                                           |Iman|
|[Product design](https://gitlab.com/bozmeg/inovation-textile/wikis/Product-design)                                                                             |Iman|
