/**
 * Boson - boson_library.h
 *     Protocol description
 * @author Ohad Nathan<ohad@me.com>
 */
#include "application.h"

//Protocol calls
#define P_SYN  0x21     //photon is looking for the baseStation " ! "
#define P_ACK  0x22     //baseStation handshake                 " " "
#define P_DATA 0x23     //a message with pulse data             " # "
#define P_EOTC 0x2F     //a message with breath data            " / "
#define P_READ 0x24     //listening mode                        " $ "
#define P_STOP 0x25     //sleep                                 " % "
#define P_RSET 0x26     //restart                               " & "
#define P_DBGO 0x28     //debug on                              " ( "
#define P_DBGX 0x29     //debug off                             " ) "


//state calls
#define S_INIT 0x00  //sends a sync request to the base station " 0 "
#define S_READ 0x01  //reads incoming packet                    " 1 "
#define S_DATA 0x02  //transmitte sensor data                   " 2 "
#define S_STOP 0x03  //shutdown                                 " 3 "
#define S_RSET 0x04  //restart                                  " 4 "
#define S_DBUG 0x05  //debug                                    " 5 "
