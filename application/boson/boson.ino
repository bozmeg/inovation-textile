/**
 * Boson - boson.ino
 *     Project description
 * @author Ohad Nathan<ohad@me.com>
 */
#include "boson_library.h"

// sets the max size of the buffer
#define BUFFER_SIZE 1024
#define TIME        0
#define VALUE       1

// wifi only mode
//SYSTEM_MODE(SEMI_AUTOMATIC);

// pin setting for pulse sensor
int pulsePin = A0;
// pin setting for breathing sensor
int breathPin = A1;
// debug led (its there for fun)
int LED = D7;


//Port number for incoming UDP packet.
unsigned int localPort = 4200;
// port number for outgoing UDP packet
unsigned int remotePort = 4201; //TODO: make diynamic
// unsigned int representing the state of the device
unsigned int state = S_INIT;
// define buffer as char array with a max size
char buffer[BUFFER_SIZE];

// counters for debuging packat lost
unsigned int packetInCount = 0;
unsigned int packetOutCount = 0;

// array to hold pulse sensor raw and potential peak data
int sensorDataArray[2][2] = {};
int potentialPeak[2] = {};

// timeout counters for pulse sensor peak detection
unsigned int timeOutCounter;
const int maxTimeOut = 3;

// direction state of the pulse sensor 2 means going up 1 means going down
unsigned int directionState = 2;

// base station ip
IPAddress baseStation;
// multicast ip to AP boradcast address
IPAddress multicast(192,168,2,255);
// starts the udp service
UDP Udp;

//debug toggle
bool debug = false;

/**
 * prints debug information to the serial consloe
 */
void logger() {
  Serial.print("buffer: ");
  Serial.println(buffer);
  Serial.print("Packets In: ");
  Serial.print(packetInCount, DEC);
  Serial.print(" Packets Out: ");
  Serial.println(packetOutCount, DEC);
  Serial.print(" State: ");
  Serial.println(state);
  Serial.print(" timeOutCounter: ");
  Serial.println(timeOutCounter);
}
/**
 * Sends a packet to the requested remoteAddress on remotePort.
 * @param remoteAddresss The remote address
 * @param remotePort     The remote port
 * @param message        The message to send
 */
void sendPacket(IPAddress remoteAddress, unsigned int remotePort, String message) {
  Udp.beginPacket(remoteAddress, remotePort);
  Udp.write(0x23);
  Udp.write(message);
  Udp.endPacket();
  packetOutCount += 1;
}

/**
 * Sends a packet to the requested remoteAddress on remotePort.
 * @param remoteAddresss The remote address
 * @param remotePort     The remote port
 * @param command        The command to send
 */
void sendPacket(IPAddress remoteAddress, unsigned int remotePort, int command) {
  Udp.beginPacket(remoteAddress, remotePort);
  Udp.write(command);
  Udp.endPacket();
}

/**
 * Paeses the buffer and gets the first character
 * of the incoming message.
 */
int parseBuffer() {
  if (debug) {                            //if debug is true run the logger
    logger();
  }

  char command = buffer[0];
  switch(command) {
    case P_ACK:                            //handshake from base station
      Serial.println("Got Handshake!");
      baseStation = Udp.remoteIP();       //sets base station ip to the senders
      return state = S_DATA;              //sets state to transmitte data
      break;
    case P_READ:                          //listening only command
      Serial.println("Got Read!");
      return state = S_READ;              //sets listening state
      break;
    case P_STOP:                          //shutdown command
      Serial.println("Got Stop!");
      return state = S_STOP;              //sets state to shutdown
      break;
    case P_DBGO:                          //set debug to true
      debug = true;
      Serial.println("Debug: True");
      return state;
      break;
    case P_DBGX:                          //set debug to false
      debug = false;
      Serial.println("Debug: false");
      return state;
      break;
    case P_RSET:                          //reset command
      Serial.println("Got Reset");
      return state = S_RSET;              //set state to reset
      break;
    default:
      return state;
  }
  if (debug) {                            //if Debug is true echo state to sender
    sendPacket(baseStation, remotePort, "Changed to state: " + state);
  }
}

/**
 * Reads a incoming UDP packet on the local port then loads it
 * into the buffer.
 */
void readPacket () {
  if (Udp.parsePacket() > 0){
    Udp.read(buffer, BUFFER_SIZE);
    packetInCount += 1;
    parseBuffer();
  }
}

/**
 * Reads sensors
 *
 */
int readSensorData () {
  // reads pulse sensor pin
  int beat = analogRead(pulsePin);
  // reads breathing sensor pin
  int breath = analogRead(breathPin);
  //reads the time now in milliseconds
  unsigned long coreTime = millis();

  //shifts old pulse data from slot 0 to slot 1
  sensorDataArray[1][VALUE] = sensorDataArray[0][VALUE];
  sensorDataArray[1][TIME] = sensorDataArray[0][TIME];

  //sets new pulse data to slot 0
  sensorDataArray[0][VALUE] = beat;
  sensorDataArray[0][TIME]  = coreTime;

  //sets the current directional trend
  int direction = sensorDataArray[0][VALUE] - sensorDataArray[1][VALUE];

  // increasing and was increasing before
  if (direction >= 0 && directionState == 2) {
    directionState = 2;                       //going up
    timeOutCounter = maxTimeOut;              //reset timeout counter
  }

  // increasing and was decreasing before
  else if (direction >= 0 && directionState == 1) {
    directionState = 2;                       //going up
    //if this is going up after going down and it has a potentialPeak
    if (potentialPeak[VALUE] != 0) {
      //if the timeOutCounter is still full then it is not a real peak
      if (timeOutCounter != 0 && potentialPeak[VALUE] < sensorDataArray[0][VALUE]) {
        potentialPeak[VALUE] = sensorDataArray[1][VALUE];
        potentialPeak[TIME] = sensorDataArray[1][TIME];
        timeOutCounter = maxTimeOut;
      }
      else {
        timeOutCounter -= 1;
      }
    }
  }
      // decreasing and was increasing before <---------   potentialPeak
  else if (direction <= 0 && directionState == 2) {
    directionState = 1;                       //going down
    // if you dont have a potential peak
    if (potentialPeak[VALUE] == 0) {
      potentialPeak[VALUE] = sensorDataArray[1][VALUE];
      potentialPeak[TIME] = sensorDataArray[1][TIME];
      timeOutCounter = maxTimeOut;
    }
    else if (potentialPeak[VALUE] < sensorDataArray[1][VALUE]) {
      potentialPeak[VALUE] = sensorDataArray[1][VALUE];
      potentialPeak[TIME] = sensorDataArray[1][TIME];
      timeOutCounter = maxTimeOut;
    }
    else if (potentialPeak[VALUE] > sensorDataArray[1][VALUE]) {
      timeOutCounter -= 1;
      if (timeOutCounter == 0) {
        sendPacket(baseStation, remotePort,
          String(potentialPeak[TIME]) + "," + String(breath));
        packetOutCount += 1;
        potentialPeak[TIME] = 0;
        potentialPeak[VALUE] = 0;
        timeOutCounter = maxTimeOut;
      }
    }
  }
  // decreasing and was decreasing before
  else if (direction <= 0 && directionState == 1) {
    directionState = 1;
    //if this is going down after going down and it has a potentialPeak timeOutCounter - 1
    if (potentialPeak[VALUE] != 0) {
      timeOutCounter -= 1;
      //if the timeOutCounter is still empty then it is a real peak
      if (timeOutCounter == 0) {
        sendPacket(baseStation, remotePort,
          String(potentialPeak[TIME]) + "," + String(breath));
        packetOutCount += 1;
        potentialPeak[TIME] = 0;
        potentialPeak[VALUE] = 0;
        timeOutCounter = maxTimeOut;
      }
    }
  }
}


/**
 * Particle setup function.
 */
void setup() {
    //WIFI only mode
    //WiFi.connect();
    //set led pin to output
    pinMode(LED, OUTPUT);
    //set serial and start
    Serial.begin(115200);
    // start listening for UDP on packet on the local port
    Udp.begin(localPort);
}

/**
 * Particle loop function.
 */
void loop() {
  // handle the different states
  switch (state) {
    case S_INIT: // init state
      sendPacket(multicast, remotePort, P_SYN);
      delay(200);
    case S_READ: // listening state
      readPacket();
      //delay(200);
      break;
    case S_DATA: //transmitte state
      readPacket();
      readSensorData();
      delay(40);
      break;
    case S_STOP: // stop state
      System.sleep(SLEEP_MODE_DEEP,10);
    case S_RSET: // stop state
      for (int i = 0; i <= 4; i++) {
        digitalWrite(LED, HIGH);
        delay(100);
        digitalWrite(LED, LOW);
      }
      System.reset();
      break;
    default:
      Serial.println("Undifined State");
      state = S_INIT;
      break;
  }
}
