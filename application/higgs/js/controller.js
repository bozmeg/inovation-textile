/**
 * Higgs - controller.js
 *     Project description
 * @author Ohad Nathan<ohad@me.com>
 */

// includes
var $ = require('jquery-latest');     //jquery
var gui = require('nw.gui');          //nwjs gui controller
var debugMode = false;                //state toggle for debug

/**
 * This method gets triggered when the DOM has been loaded
 */
$(document).ready(function() {
  // create a chart and time line and connects it to the canvas
  var chart = new SmoothieChart({millisPerPixel:39,grid:{fillStyle:'rgba(85,91,103,0.64)',sharpLines:true,millisPerLine:1000,verticalSections:8},labels:{fontSize:12},timestampFormatter:SmoothieChart.timeFormatter}),
      canvas = document.getElementById('mycanvas'),
      beatSeries = new TimeSeries();
      breathSeries = new TimeSeries();

/**
 * updates the chart with pulse sensor data every 1 second
 */
  setInterval(function() {
    beatSeries.append(new Date().getTime(), BeatsPerMin);
    breathSeries.append(new Date().getTime(), breathData);
  }, 1000);

  //add the time series to the chart
  chart.addTimeSeries(beatSeries, {lineWidth:2,strokeStyle:'#e01e1e',fillStyle:'rgba(0,0,0,0.10)'});
  chart.addTimeSeries(breathSeries, {lineWidth:2,strokeStyle:'#a6ce39',fillStyle:'rgba(0,0,0,0.10)'});
  chart.streamTo(canvas, 605);

  /**
   * This method gets triggered when the connect button has been clicked
   */
  $('.button').click(function(){
    //if the application is not connected do handshake and set the button to connected mode
    if ($(this).hasClass('connect')) {
      handShake();
      $(this).addClass('disconnect').removeClass('connect').text('Disconnect');
      return;
    }
    //if the application is connected reset the core and set the button to disconnected mode
    if ($(this).hasClass('disconnect')) {
      resetCore();
      $(this).addClass('init').removeClass('disconnect').text('Core not found!');
      return;
    }
    //if the application does not detect a core the buttons wont work
    if ($(this).hasClass('init')) {
      alert('No core detected!');
      return;
    }

  });

  /**
   * This method gets triggered when any button other then connect/Disconnect has been clicked
   */
  $('.buttonContainer button').click(function() {
    //if not in init state pass this button to button Function
    if (state != 0) {
      buttonFunctions[$(this).attr("name")](this);
    }
    //if in init mode then no core has been found
    else {
      alert('No core detected!');
    }
  });

});
/**
 * [buttonFunctions holds a lists of methods for footer button functionality  ]
 * @type {Object}
 */
var buttonFunctions = {
  /**
   * [toggles core listening mode]
   * @method listen
   * @param  {object} button [the button clicked by the user]
   */
  listen: function (button) {
    //if listening is ture disconnect
    if ($(button).hasClass('disconnect')) {
      resetCore();
      $(button).removeClass('disconnect');
    }
    //if listening is false connect
    else {
      $(button).addClass('disconnect');
      listenOnly();
      console.log("listen");
    }
  },

  /**
   * [Puts core in sleep mode to save power]
   * @method sleep
   * @param  {object} button [the button clicked by the user]
   */
  sleep: function (button) {
    sleepMode();
    alert('No core detected!');
  },

  /**
   * [toggle core debug mode]
   * @method debug
   * @param  {object} button [the button clicked by the user]
   * @return {[bool]}        [debugMode]
   */
  debug: function (button) {
    //if in debug mode disable it
    if (debugMode) {
      debugMode = disableDebugMode();
      $(button).removeClass('disconnect')
      console.log('true' + debugMode);
    }
    //if not in debug mode enabole it
    else {
      debugMode = enableDebugMode();
      $(button).addClass('disconnect');
      console.log('false' + debugMode);
    }
  },

  /**
   * [resets the core to init and quits the application]
   * @method power
   * @param  {object} button [the button clicked by the user]
   */
  power: function (button) {
    resetCore();
    gui.App.quit();
  }
};

/**
 * [updates the ui when a core is detected]
 * @method updateUI
 */
var updateUI = function () {
  if ($('.button').hasClass('init')) {
    $('.button').addClass('connect').removeClass('init').text('Connect');
    $('.deviceInfo > span:first-child').text('Core IP: ' + coreAddress);
    $('.deviceInfo > span:last-child').text('Core Port: ' + corePort);
    return;
  }
//TODO:update when device stops sending data
};

var disconnectHandler = function() {
  state = 0;
  $('.button').addClass('init').removeClass('connect disconnect').text('Core disconnect!');
  $('.deviceInfo > span:first-child').text('Core IP: ' + 'Connection lost!');
  $('.deviceInfo > span:last-child').text('Core Port: ' + 'Connection lost!');
}
