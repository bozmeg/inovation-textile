/**
 * Higgs - UDPserver.js
 *     Project description
 * @author Ohad Nathan<ohad@me.com>
 */

//includes
var dgram = require('dgram');                 //UDP server client
//declarations
var PORT = 4201;                              //server port address
var HOST;                                     //server ip address

var server = dgram.createSocket('udp4');      //binds server to use UDP4
var client = dgram.createSocket('udp4');      //binds client to use UDP4

var data;                                     //incoming pulse data
var breathData;                               //incoming breathing data
var command = new Buffer(256);                //commends buffer
var inMessage;                                //holds incoming UDP dgram

var pulseData = [];                           //array for holding detected peaks
var BeatsPerMin = 0;                          //holds calculated BPM

var coreAddress;                              //remote cilent address
var corePort;                                 //remote cilent port
var inCounter = 0;                            //counter for incoming dgrams
var outCounter = 0;                           //counter for outgoing dgrams
var state = 0;                                //server state 0 = init


/**
 * [called when receiving incoming pulse data it reads]
 * [the time of the last detected peak and a one previous peak and calculates]
 * [the BPM by subtracting the time of the previous peak from the first peak]
 * [dividing this time by the number of peaks in this array (three) and then multiplying]
 * [the result by 60000 milliseconds]
 * @method function
 * @param  {[int]} data [holds last pulse peaks timestamp]
 * @return {[int]}      [BeatsPerMin]
 */
var calculateBPM = function(data) {
  pulseData.unshift(data);
  console.log("RAW: " + data + " BPM: " + JSON.stringify(BeatsPerMin, 0, 2));
  if (pulseData.length > 3) {
    BeatsPerMin  = 3 / (pulseData[0] - pulseData[2]) * 60000;
  }
};
/**
 * [called when receiving an incoming dgram according to the type icoming message  ]
 * [the state of the server willl change state 1 will allow handshake and update]
 * [the UI, state 2 will convert incoming dgrams to int called data]
 * @method parseData
 * @return {[int]}  [state]
 * @return {[int]}  [data]
 */
var parseData = function () {

  switch (inMessage.charAt(0)) {
    case '!':                               //core on network broadcasting handshake
      state = 1;
      updateUI();                           //update ui to show core has been found
      break;
    case '#':                               //core on network broadcasting pulsh data

      for (var i = 0; i < inMessage.length; i++) {          // looks for any breathing data messages
        if (inMessage[i] == "/x40") {                       // if there is a "," character in the message
          var spliceMessage = inMessage.splice(i);          //cut the array from that point
          var rawBreathData = spliceMessage.substring(1);   //takes all but the first character
          breathData = parseInt(rawBreathData);             //turns the string into an int
        }
      }
      var rawData = inMessage.substring(1);     //takes all but the first characters in the dgram string
      data = parseInt(rawData);                 //turns the string into an int
      calculateBPM(data);

      if (state != 2) {                         // if state is not data and data is being resived
          state = 2;                            // set state to data state
      }
      break;

    default:
      console.log("Undefined message header"); //error thrown
  }
};

/**
 * [sends a handShake command to the core client ]
 * @method handShake
 */
var handShake = function () {
  //if there is a core client with a valid ip broadcasting an init signal
  if (coreAddress.length > 0 && inMessage == '!') {
    command.write("\x22");      //write escape character " ! " to buffer
    client.send(command, 0, command.length, corePort, coreAddress);
    //console.log(coreAddress + ' :Core ' + corePort + ' :Port ' + command + ' :command')
  }
};

/**
 * [sends a reset command to the core client and sets state to init]
 * @method resetCore
 * @return {[int]}   [init state]
 */
var resetCore = function () {
    command.write("\x26");    //write escape character " & " to buffer
    client.send(command, 0, command.length, corePort, coreAddress);
    state = 0;
};

/**
 * [sends a listen only mode command to the core client and sets state to core available]
 * @method listenOnly
 * @return {[int]}   [core available state]
 */
var listenOnly = function () {
  command.write("\x24");    //write escape character " $ " to buffer
  client.send(command, 0, command.length, corePort, coreAddress);
  state = 1;
};

/**
 * [sends a sleep command to the core client and sets state to init]
 * @method sleepMode
 * @return {[int]}  [init state]
 */
var sleepMode = function () {
  command.write("\x25");    //write escape character " % " to buffer
  client.send(command, 0, command.length, corePort, coreAddress);
  state = 0;
};

/**
 * [sends an enableDebugMode command]
 * @method enableDebugMode
 * @return {[bool]}        [true]
 */
var enableDebugMode = function () {
  command.write("\x28");    //write escape character " ( " to buffer
  client.send(command, 0, command.length, corePort, coreAddress);
  return true;
};

/**
 * [sends an disableDebugMode command]
 * @method disableDebugMode
 * @return {[bool]}         [false]
 */
var disableDebugMode = function () {
  command.write("\x29");  //write escape character " ) " to buffer
  client.send(command, 0, command.length, corePort, coreAddress);
  return false;
};

/**
 * [starts the udp server]
 * @method server.on.listening
 * @method  {[object]} 'listening' [listens on server port for incoming UDP packates]
 * @return {[bool]}             [true]
 * @return {[string]}           [server ip]
 */
server.on('listening', function () {
    HOST = server.address();
    //console.log('UDP Server listening on ' + server.address + ":" + server.port);
});

/**
 * [runes when a udp message is resived ]
 * @method server.on.message
 * @method  {[object]} 'message' [buffer for incoming dgram]
 * @param  {[type]} function  (message,     remote [description]
 * @return {[buffer]}         [holds incoming message]
 * @return {[object]}         [holds remote ip and port address]
 */
server.on('message', function (message, remote) {
  inMessage = message.toString('ascii');    //turns the dgram into a string
  coreAddress = remote.address;             //sets client ip to core address
  corePort = remote.port;                   //sets client port to core port
  parseData();
  //console.log("Time: " + inMessageTime);
});

server.bind(PORT);                          //binds the server to port 4201
